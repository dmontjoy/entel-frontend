export const environment = {
  production: true,
  apiUrl: 'http://localhost:8080/api/data',
  termLength: +3,
  apiUrlBase:'http://localhost:8080/',
};
