import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { environment } from 'src/environments/environment';
import { RestangularModule } from 'ngx-restangular';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

export function RestangularConfigFactory (RestangularProvider, authService) {
  RestangularProvider.setBaseUrl(environment.apiUrl);

  RestangularProvider.addResponseInterceptor((data, operation, what, url, response) => {
    if (operation === 'get') {
      let resp;

      switch (typeof data) {
        case 'object':
          resp = data;
          break;

        default:
          resp = {data : data};
          break;
      }

      return resp;
    }

    if (operation === 'getList') {
      let resp = data._embedded ? data._embedded[what] : data;

      // Sino coincide el path con la propiedad, busco un array en la respuesta.
      if (resp === undefined) {
        if ( Array.isArray(data._embedded) ) {
          resp = data._embedded;
        } else if ( data._embedded && typeof data._embedded === 'object' ) {
            for (const key in data._embedded) { // Recorro las propiedades del objeto hasta encontrar un array
                if ( Array.isArray(data._embedded[key]) && resp === undefined ) {
                    resp = data._embedded[key];
                }
            }
        }
      }

      resp._links = data._links || null;
      resp.page = data.page || null;
      return resp;
    }

    return data;
  });

  RestangularProvider.setRestangularFields({
    selfLink: '_links.self.href',
  });
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DashboardModule,
    HttpClientModule,
    RestangularModule.forRoot( RestangularConfigFactory),
    LoggerModule.forRoot({level: NgxLoggerLevel.DEBUG}),
    FormsModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
