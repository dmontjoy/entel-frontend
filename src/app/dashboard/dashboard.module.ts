import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TagInputModule } from 'ngx-chips';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { PrincipalComponent } from './principal/principal.component';
import { DashboardComponent } from './dashboard.component';
import { NodeGraphComponent } from './node-graph/node-graph.component';

import { D3Service, D3_DIRECTIVES } from '../shared/d3';
import { GraphComponent } from '../shared/visuals/graph/graph.component';
import { SHARED_VISUALS } from '../shared/visuals/shared';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    DashboardComponent,
    PrincipalComponent,
    GraphComponent,
    ...SHARED_VISUALS,
    ...D3_DIRECTIVES,
    NodeGraphComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    NgbModule,
    FormsModule,
    TagInputModule,
  ],
  providers: [D3Service],
})
export class DashboardModule { }
