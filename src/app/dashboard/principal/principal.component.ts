import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap, switchMap, catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Restangular } from 'ngx-restangular';
import { NGXLogger } from 'ngx-logger';
import * as _ from 'lodash';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.scss']
})
export class PrincipalComponent implements OnInit {
  nodoTypeahead:  any;
  searching:      boolean = false;
  searchFailed:     boolean = false;
  nodos:       any[]=[];
  caminos:       any[]=[];
  apiUrlBase= environment.apiUrlBase;
  constructor(    
    private restangular: Restangular,
    private logger: NGXLogger,) { }

  ngOnInit() {

  }
  searchNodo = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(300),
    distinctUntilChanged(),
    tap(() => this.searching = true),
    switchMap(nodoNombre => nodoNombre.length < environment.termLength ? [] :
      this.restangular.all('nodos').one('search').getList('allByNodoNombre', {nodoNombre: nodoNombre}).pipe(
        tap(() => this.searchFailed = false),
        catchError(() => {
          this.searchFailed = true;
          return of([]);
        }),
      ),
    ),
    tap(() => this.searching = false),
  );
  formatterNodo = (n: {nombre: string}) => n.nombre;

  selectNodo ($event) {
    $event.preventDefault();

    const find = _.find(this.nodos, function(item) {
      return item.id === $event.item.id;
    });

    if (!find) {
      this.nodos.push($event.item.plain());
      
    }
  }

  search() {
    let idNodos, nombreNodos;
    
    idNodos     = _.map(this.nodos, 'id');
    nombreNodos     = _.map(this.nodos, 'nombre');
    this.logger.debug(idNodos);
    this.restangular.all('caminos').one('search').getList('allByNameCamino', {idNodo: nombreNodos, projection: 'camino'},
    ).subscribe(response => {
      this.caminos = response;
      window.open('http://localhost:8080/grafo?idNodo='+nombreNodos, '_blank');
  });
  }

}