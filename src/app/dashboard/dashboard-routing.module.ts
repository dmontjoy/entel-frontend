import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { PrincipalComponent } from './principal/principal.component';
import { NodeGraphComponent } from './node-graph/node-graph.component';

const routes: Routes = [  {
  path: '',
  component: DashboardComponent,
  children: [
    {path: '', redirectTo: 'list', pathMatch: 'full'},
    {path: 'list', component: PrincipalComponent},
    {path: 'graph', component: NodeGraphComponent}
  ],
},];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
